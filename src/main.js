import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueToastr from "vue-toastr"
import VueMask from 'v-mask'
import { VueMaskDirective } from 'v-mask'

Vue.config.productionTip = false
Vue.use(VueToastr)
Vue.use(VueMask)
Vue.directive('mask', VueMaskDirective);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
