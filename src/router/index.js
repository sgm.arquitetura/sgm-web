import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import {Roles} from '../modulos.js'

import Home from '../views/Home.vue'

import PublicLayout from '../components/Layout/PublicLayout.vue'
import AdminLayout  from '../components/Layout/AdminLayout.vue'

import AuthHome from '../views/AuthHome.vue'
import Usuarios from '../views/administracao/usuario/Usuarios.vue'
import Usuario from '../views/administracao/usuario/Usuario.vue'
import OrgaosMunicipais from '../views/administracao/orgaos-municipais/OrgaosMunicipais.vue'
import ConsultaIPTU from '../views/cidadao/ConsultaIPTU.vue'
import ConsultaIPTR from '../views/cidadao/ConsultaIPTR.vue'
import ConsultaImoveis from '../views/cidadao/ConsultaImoveis.vue'

import VisualizaMapas from '../views/georreferenciamento/VisualizaMapa.vue'
import Projetos from '../views/projeto/Projetos.vue'
import GeraRelatorio from '../views/relatorio/GeraRelatorio.vue'

import Profile from '../views/profile/Profile.vue'
import AcessoNegado from '../views/AcessoNegado.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: PublicLayout,
    children:[
      {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
          requireAuth: false,
          roles:[]
        }
      }
    ]
  },
  {
    path: '/',
    component: AdminLayout,
    children:[
      {
        path: '/auth',
        name: 'AuthHome',
        component: AuthHome,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_CIDADAO, Roles.ROLE_ADMINISTRADOR, Roles.ROLE_OPERADOR_PREFEITURA, Roles.ROLE_OPERADOR_ORGAO_MUNICIPAL]
        }
      },
      {
        path: '/auth/usuarios',
        name: 'Usuarios',
        component: Usuarios,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_ADMINISTRADOR]
        }
      },
      {
        path: '/auth/usuario/:id(\\d+|novo)',
        name: 'Usuario',
        component: Usuario,
        props: true,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_ADMINISTRADOR]
        }
      },
      {
        path: '/auth/orgaos-municipais',
        name: 'OrgasMunicipais',
        component: OrgaosMunicipais,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_ADMINISTRADOR]
        }
      },
      {
        path: '/auth/iptu',
        name: "IPTU",
        component:ConsultaIPTU,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_CIDADAO, Roles.ROLE_ADMINISTRADOR, Roles.ROLE_OPERADOR_PREFEITURA, Roles.ROLE_OPERADOR_ORGAO_MUNICIPAL]
        }
      },
      {
        path: '/auth/iptr',
        name: "IPTR",
        component:ConsultaIPTR,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_CIDADAO, Roles.ROLE_ADMINISTRADOR, Roles.ROLE_OPERADOR_PREFEITURA, Roles.ROLE_OPERADOR_ORGAO_MUNICIPAL]
        }
      },
      {
        path:'/auth/mapas',
        name: 'VisualizaMapas',
        component:VisualizaMapas,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_OPERADOR_PREFEITURA, Roles.ROLE_OPERADOR_ORGAO_MUNICIPAL]
        }
      },
      {
        path: '/auth/projetos',
        name: 'Projetos',
        component: Projetos,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_OPERADOR_PREFEITURA, Roles.ROLE_OPERADOR_ORGAO_MUNICIPAL]
        }
      },
      {
        path: '/auth/relatorios',
        name: 'Relatorios',
        component: GeraRelatorio,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_ADMINISTRADOR, Roles.ROLE_OPERADOR_PREFEITURA, Roles.ROLE_OPERADOR_ORGAO_MUNICIPAL]
        }
      },
      {
        path: '/auth/imoveis',
        name: 'ConsultaImoveis',
        component: ConsultaImoveis,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_CIDADAO, Roles.ROLE_ADMINISTRADOR, Roles.ROLE_OPERADOR_PREFEITURA, Roles.ROLE_OPERADOR_ORGAO_MUNICIPAL]
        }
      },
      {
        path: '/auth/profile',
        name: 'Profile',
        component: Profile,
        meta: {
          requireAuth: true,
          roles:[Roles.ROLE_CIDADAO, Roles.ROLE_ADMINISTRADOR, Roles.ROLE_OPERADOR_PREFEITURA, Roles.ROLE_OPERADOR_ORGAO_MUNICIPAL]
        }
      },
      {
        path: '/acesso-negado',
        name: 'AcessoNegado',
        component: AcessoNegado,
        meta: {
          requireAuth: false,
          roles:[[Roles.ROLE_CIDADAO, Roles.ROLE_ADMINISTRADOR, Roles.ROLE_OPERADOR_PREFEITURA, Roles.ROLE_OPERADOR_ORGAO_MUNICIPAL]]
        }
      }

    ]
  }  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((routeTo, routeFrom, next)=>{
  if(!routeTo.meta.requireAuth){ return next() }

  store.dispatch('buscarDadosUsuario')
    .then( ()=> {
      const roles = store.getters.roles
      if(roles) {
        const rolesinterception = roles.filter(r => routeTo.meta.roles.indexOf(r) >= 0)
        if (rolesinterception.length === 0) { return next('/acesso-negado') }
      }
      return next();
    })
    .catch(()=> { return next("/"); })
})

export default router
