import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'
  

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null,
    usuario: {}
  },
  mutations: {
    DEFINIR_TOKEN(state, token) {
      state.token = token
      state.autenticado= true
    },
    DEFINIR_USUARIO(state, usuario) {
      state.usuario = usuario
    },
    DESLOGAR_USUARIO(state) {
      state.token = null;
      state.usuario = {}
      localStorage.removeItem('sgm.token')
    }
  },
  actions: {
    efetuarLogin({commit}, usuario) {
      return new Promise((resolve,reject)=> {
        const formData = new FormData();
        formData.append('scope','web')
        formData.append('grant_type','password')
        formData.append('username', usuario.email)
        formData.append('password', usuario.senha)
        
        const config = {
          headers:{ Authorization: `Basic c2dtOnNnbXB3ZA==` }
        }

        axios.post("http://localhost:5555/identity/oauth/token", formData, config)
              .then(response => {
                const token = `${response.data.token_type} ${response.data.access_token}`
                commit('DEFINIR_TOKEN', token)
                localStorage.setItem('sgm.token', token)
                resolve(response.data)
              })
              .catch(err => {
                reject(err)
              })
      })
    },
    buscarDadosUsuario({commit, state}) {
      return new Promise((resolve, reject)=> {
        const localToken = localStorage.getItem('sgm.token');
        const token = localToken ? localToken : state.token
        
        const config = {
          headers:{ Authorization: token }
        }
        axios.get("http://localhost:5555/identity/users/auth", config)
            .then(response => {
              commit('DEFINIR_TOKEN', token)
              commit('DEFINIR_USUARIO', response.data)
              resolve(response.data)
            })
            .catch(err => {
              commit('DESLOGAR_USUARIO', token)
              reject(err)
            })
      })
    }
  },
  getters: {
    token: state => {
      return state.token
    },
    username: state => {
      return state.usuario.nome
    },
    roles: state => {
      return state.usuario.perfis
    }
  }
})