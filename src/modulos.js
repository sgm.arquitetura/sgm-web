const ROLE_CIDADAO = "CIDADAO"
const ROLE_ADMINISTRADOR = "ADMINISTRADOR"
const ROLE_OPERADOR_PREFEITURA = "OPERADOR_PREFEITURA"
const ROLE_OPERADOR_ORGAO_MUNICIPAL = "OPERADOR_ORGAO_MUNICIPAL"

const Modulos = [
    { 
      codigo:"ADMINISTRACAO", 
      descricao:"Administração do Sistema",
      icone:"gear",
      cor:"danger",
      links:[
          {nome:"Usuários", url:"/auth/usuarios"},
          {nome:"Órgãos Municipais", url:"/auth/orgaos-municipais"}
      ],
      roles: [ROLE_ADMINISTRADOR]
  },
  { 
      codigo:"CIDADAO",  
      descricao:"Cidadão",  
      cor:"primary", 
      icone:"person", 
      links:[
          {nome:"Consultar Relação de Imóveis", url:"/auth/imoveis"},
          {nome:"Consultar IPTU", url:"/auth/iptu"},
          {nome:"Consultar IPTR", url:"/auth/iptr"}
      ],
      roles: [ROLE_ADMINISTRADOR, ROLE_CIDADAO,ROLE_OPERADOR_PREFEITURA, ROLE_OPERADOR_ORGAO_MUNICIPAL]
  },
  { 
      codigo:"GEORREFERENCIAMENTO",
      descricao:"Georreferenciamento", 
      cor:"success", 
      icone:"map-fill", 
      links:[
          {nome:"Visualizar Mapas", url:"/auth/mapas"}
      ],
      roles: [ROLE_OPERADOR_PREFEITURA, ROLE_OPERADOR_ORGAO_MUNICIPAL]
  },
  { 
      codigo:"PROJETOS", 
      descricao:"Projetos", 
      cor:"dark", 
      icone:"bar-chart-steps",
      links:[{nome:"Acessar Projetos", url:"/auth/projetos"}],
      roles: [ROLE_OPERADOR_PREFEITURA, ROLE_OPERADOR_ORGAO_MUNICIPAL]
  },
  { 
      codigo:"RELATORIOS", 
      descricao:"Relatórios", 
      icone:"clipboard-data", 
      cor:"warning", 
      links:[{nome:"Gerar Relatório", url:"/auth/relatorios"}],
      roles: [ROLE_ADMINISTRADOR, ROLE_OPERADOR_PREFEITURA, ROLE_OPERADOR_ORGAO_MUNICIPAL]
  }]

export const Roles = Object.freeze({
    ROLE_CIDADAO,
    ROLE_ADMINISTRADOR,
    ROLE_OPERADOR_PREFEITURA,
    ROLE_OPERADOR_ORGAO_MUNICIPAL
})

export default Modulos